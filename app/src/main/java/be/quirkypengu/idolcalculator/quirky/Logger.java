package be.quirkypengu.idolcalculator.quirky;

import android.util.Log;

/**
 * @author joeriverlooy
 * @version 1.0
 * @created 07/06/2017
 */

public class Logger {
    public static void i(String tag, String message) {
        if (Settings.DEBUG) {
            Log.i(tag, message);
        }
    }

    public static void e(String tag, String message) {
        if (Settings.DEBUG) {
            Log.e(tag, message);
        }
    }

    public static void d(String tag, String message) {
        if (Settings.DEBUG) {
            Log.d(tag, message);
        }
    }
}
