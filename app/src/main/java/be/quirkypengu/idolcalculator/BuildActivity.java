package be.quirkypengu.idolcalculator;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import javax.inject.Inject;

import be.quirkypengu.idolcalculator.data.helpers.DataHelper;
import be.quirkypengu.idolcalculator.data.repository.BossRepository;
import be.quirkypengu.idolcalculator.data.repository.GradeRepository;
import be.quirkypengu.idolcalculator.data.repository.IdolRepository;
import be.quirkypengu.idolcalculator.data.repository.IncompatibilityRepository;
import be.quirkypengu.idolcalculator.data.repository.SynergyRepository;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 24/05/2017
 */

public class BuildActivity extends AppCompatActivity {

    @Inject
    GradeRepository gradeRepository;

    @Inject
    BossRepository bossRepository;

    @Inject
    IdolRepository idolRepository;

    @Inject
    IncompatibilityRepository incompatibilityRepository;

    @Inject
    SynergyRepository synergyRepository;

    DataHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_build);
        ((IdolCalculatorApplication)getApplication()).getComponent().inject(this);
        helper = new DataHelper();
        new InitializeDataTask().execute();
    }

    private class InitializeDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            helper.initBossData(bossRepository);
            helper.initGradeData(gradeRepository);
            helper.initIdolData(idolRepository);
            helper.initIncompatibilityData(incompatibilityRepository);
            helper.initSynergyData(synergyRepository);
            return null;
        }
    }
}
