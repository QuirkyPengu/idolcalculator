package be.quirkypengu.idolcalculator;

import android.app.Application;
import android.provider.ContactsContract;

import be.quirkypengu.idolcalculator.injection.DaggerDataComponent;
import be.quirkypengu.idolcalculator.injection.DataComponent;
import be.quirkypengu.idolcalculator.injection.DataModule;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 29/05/2017
 * @modified
 * @updated
 */

public class IdolCalculatorApplication extends Application {

    private DataComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerDataComponent.builder()
                .dataModule(new DataModule(this))
                .build();
    }

    public DataComponent getComponent() {
        return mComponent;
    }

}
