package be.quirkypengu.idolcalculator.injection;

import javax.inject.Singleton;

import be.quirkypengu.idolcalculator.BuildActivity;
import be.quirkypengu.idolcalculator.IdolCalculatorApplication;
import be.quirkypengu.idolcalculator.builds.BuildsViewModel;
import be.quirkypengu.idolcalculator.editbuild.EditBuildViewModel;
import dagger.Component;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 2/06/2017
 * @modified
 * @updated
 */
@Singleton
@Component(modules = {DataModule.class})
public interface DataComponent {
    void inject(IdolCalculatorApplication app);
    void inject(BuildsViewModel viewModel);
    void inject(EditBuildViewModel viewModel);
    void inject(BuildActivity activity);

    interface Injectable {
        void inject(DataComponent component);
    }
}
