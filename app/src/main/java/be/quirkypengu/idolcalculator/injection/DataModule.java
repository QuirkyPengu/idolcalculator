package be.quirkypengu.idolcalculator.injection;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import be.quirkypengu.idolcalculator.IdolCalculatorApplication;
import be.quirkypengu.idolcalculator.data.db.IdolCalculatorDatabase;
import be.quirkypengu.idolcalculator.data.repository.BossRepository;
import be.quirkypengu.idolcalculator.data.repository.BossRepositoryImpl;
import be.quirkypengu.idolcalculator.data.repository.BuildLineRepository;
import be.quirkypengu.idolcalculator.data.repository.BuildLineRepositoryImpl;
import be.quirkypengu.idolcalculator.data.repository.BuildRepository;
import be.quirkypengu.idolcalculator.data.repository.BuildRepositoryImpl;
import be.quirkypengu.idolcalculator.data.repository.GradeRepository;
import be.quirkypengu.idolcalculator.data.repository.GradeRepositoryImpl;
import be.quirkypengu.idolcalculator.data.repository.IdolRepository;
import be.quirkypengu.idolcalculator.data.repository.IdolRepositoryImpl;
import be.quirkypengu.idolcalculator.data.repository.IncompatibilityRepository;
import be.quirkypengu.idolcalculator.data.repository.IncompatibilityRepositoryImpl;
import be.quirkypengu.idolcalculator.data.repository.SynergyRepository;
import be.quirkypengu.idolcalculator.data.repository.SynergyRepositoryImpl;
import dagger.Module;
import dagger.Provides;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 2/06/2017
 * @modified
 * @updated
 */

@Module
public class DataModule {

    private IdolCalculatorApplication mApp;

    public DataModule(IdolCalculatorApplication app) {
        mApp = app;
    }

    @Provides
    Context applicationContext() {
        return mApp;
    }

    @Provides
    @Singleton
    IdolCalculatorDatabase providesDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), IdolCalculatorDatabase.class, "idol_calculator_db").build();
    }

    @Provides
    @Singleton
    BossRepository providesBossRepository(IdolCalculatorDatabase db) {
        return new BossRepositoryImpl(db);
    }

    @Provides
    @Singleton
    BuildRepository providesBuildRepository(IdolCalculatorDatabase db) {
        return new BuildRepositoryImpl(db);
    }

    @Provides
    @Singleton
    BuildLineRepository providesBuildLineRepository(IdolCalculatorDatabase db) {
        return new BuildLineRepositoryImpl(db);
    }

    @Provides
    @Singleton
    GradeRepository providesGradeRepository(IdolCalculatorDatabase db) {
        return new GradeRepositoryImpl(db);
    }

    @Provides
    @Singleton
    IdolRepository providesIdolRepository(IdolCalculatorDatabase db) {
        return new IdolRepositoryImpl(db);
    }

    @Provides
    @Singleton
    IncompatibilityRepository providesIncompatibilityRepository(IdolCalculatorDatabase db) {
        return new IncompatibilityRepositoryImpl(db);
    }

    @Provides
    @Singleton
    SynergyRepository providesSynergyRepository(IdolCalculatorDatabase db) {
        return new SynergyRepositoryImpl(db);
    }

}
