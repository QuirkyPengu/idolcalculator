package be.quirkypengu.idolcalculator.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author joeriverlooy
 * @version 1.0
 * @created 28/05/2017
 */

@Entity(
        tableName = "incompatibility",
        foreignKeys = {
                @ForeignKey(entity = Idol.class,
                        parentColumns = "id",
                        childColumns = "idol_id"),

                @ForeignKey(entity = Boss.class,
                        parentColumns = "id",
                        childColumns = "boss_id")
        }
)
public class Incompatibility {
    @PrimaryKey
    private int id;
    @ColumnInfo(name = "idol_id")
    private int idolId;
    @ColumnInfo (name = "boss_id")
    private int bossId;

    @Ignore
    public Incompatibility() {
    }

    public Incompatibility(int id, int idolId, int bossId) {
        this.id = id;
        this.idolId = idolId;
        this.bossId = bossId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdolId() {
        return idolId;
    }

    public void setIdolId(int idolId) {
        this.idolId = idolId;
    }

    public int getBossId() {
        return bossId;
    }

    public void setBossId(int bossId) {
        this.bossId = bossId;
    }
}
