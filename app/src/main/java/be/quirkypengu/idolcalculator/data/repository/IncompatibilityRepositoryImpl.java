package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

import be.quirkypengu.idolcalculator.data.db.IdolCalculatorDatabase;
import be.quirkypengu.idolcalculator.data.doa.IncompatibilityDao;
import be.quirkypengu.idolcalculator.data.entities.Incompatibility;
import be.quirkypengu.idolcalculator.quirky.Logger;
import io.reactivex.Completable;
import io.reactivex.functions.Action;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 31/05/2017
 * @modified
 * @updated
 */

public class IncompatibilityRepositoryImpl implements IncompatibilityRepository {

    @Inject
    IdolCalculatorDatabase db;

    public IncompatibilityRepositoryImpl (IdolCalculatorDatabase db) {
        this.db = db;
    }

    @Override
    public LiveData<List<Incompatibility>> getIncompatibilities() {
        return db.incompatibilityDao().getIncompatibilities();
    }

    @Override
    public LiveData<Incompatibility> getIncompatibilityById(int id) {
        return db.incompatibilityDao().getIncompatibilityById(id);
    }

    @Override
    public void createIncompatibility(final Incompatibility incompatibility) {
        db.incompatibilityDao().createIncompatibility(incompatibility);
        Logger.i("Incompatibility", "Added");
    }

    @Override
    public void deleteIncompatibility(final Incompatibility incompatibility) {
        db.incompatibilityDao().deleteIncompatibility(incompatibility);
    }

    @Override
    public void updateIncompatibility(final Incompatibility incompatibility) {
        db.incompatibilityDao().updateIncompatibility(incompatibility);
    }
}
