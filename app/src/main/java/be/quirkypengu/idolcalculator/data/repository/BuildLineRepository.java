package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Build;
import be.quirkypengu.idolcalculator.data.entities.BuildLine;
import io.reactivex.Completable;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 1/06/2017
 * @modified
 * @updated
 */

public interface BuildLineRepository {
    LiveData<List<BuildLine>> getBuildLines();
    LiveData<BuildLine> getBuildLineById(int id);
    void createBuildLine(BuildLine line);
    void deleteBuildLine(BuildLine line);
    void updateBuildLine(BuildLine line);
}
