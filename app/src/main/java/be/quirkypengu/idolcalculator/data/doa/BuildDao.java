package be.quirkypengu.idolcalculator.data.doa;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Boss;
import be.quirkypengu.idolcalculator.data.entities.Build;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 1/06/2017
 * @modified
 * @updated
 */

@Dao
public interface BuildDao {
    @Query("SELECT * FROM build")
    LiveData<List<Build>> getBuilds();

    @Query("SELECT * FROM build WHERE id = :id")
    LiveData<Build> getBuildById(int id);

    @Insert
    void createBuild(Build build);

    @Delete
    void deleteBuild(Build build);

    @Update
    void updateBuild(Build build);
}
