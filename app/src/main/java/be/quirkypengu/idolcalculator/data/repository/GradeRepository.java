package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Grade;
import be.quirkypengu.idolcalculator.data.entities.Idol;
import io.reactivex.Completable;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 31/05/2017
 * @modified
 * @updated
 */

public interface GradeRepository {
    LiveData<List<Grade>> getGrades();
    LiveData<Grade> getGradeById(int id);
    void createGrade(Grade grade);
    void deleteGrade(Grade grade);
    void updateGrade(Grade grade);
}
