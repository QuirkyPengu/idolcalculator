package be.quirkypengu.idolcalculator.data.doa;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Boss;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 29/05/2017
 * @modified
 * @updated
 */

@Dao
public interface BossDao {
    @Query("SELECT * FROM boss")
    LiveData<List<Boss>> getBosses();

    @Query("SELECT * FROM boss WHERE id = :id")
    LiveData<Boss> getBossById(int id);

    @Insert
    void createBoss(Boss boss);

    @Delete
    void deleteBoss(Boss boss);

    @Update
    void updateBoss(Boss boss);
}
