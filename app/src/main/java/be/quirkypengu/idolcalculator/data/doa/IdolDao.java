package be.quirkypengu.idolcalculator.data.doa;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Idol;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 29/05/2017
 * @modified
 * @updated
 */

@Dao
public interface IdolDao {

    @Query("SELECT * FROM idol")
    LiveData<List<Idol>> getIdols();

    @Query("SELECT * FROM idol WHERE id = :id")
    LiveData<Idol> getIdolById(int id);

    @Query("SELECT * FROM idol WHERE grade_id = :id")
    LiveData<List<Idol>> getIdolsByGradeId(int id);

    @Query("SELECT * FROM idol INNER JOIN incompatibility on idol.id = incompatibility.idol_id WHERE incompatibility.boss_id = :id")
    LiveData<List<Idol>> getIdolsByBossId(int id);

    @Insert
    void createIdol(Idol idol);

    @Delete
    void deleteIdol(Idol idol);

    @Update
    void updateIdol(Idol idol);
}
