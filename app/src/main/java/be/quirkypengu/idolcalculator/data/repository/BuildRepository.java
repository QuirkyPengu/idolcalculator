package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Build;
import be.quirkypengu.idolcalculator.data.entities.Idol;
import io.reactivex.Completable;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 1/06/2017
 * @modified
 * @updated
 */

public interface BuildRepository {
    LiveData<List<Build>> getBuilds();
    LiveData<Build> getBuildById(int id);
    void createBuild(Build build);
    void deleteBuild(Build build);
    void updateBuild(Build build);
}
