package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

import be.quirkypengu.idolcalculator.data.db.IdolCalculatorDatabase;
import be.quirkypengu.idolcalculator.data.entities.Grade;
import be.quirkypengu.idolcalculator.quirky.Logger;
import io.reactivex.Completable;
import io.reactivex.functions.Action;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 31/05/2017
 * @modified
 * @updated
 */

public class GradeRepositoryImpl implements GradeRepository {

    @Inject
    IdolCalculatorDatabase db;

    public GradeRepositoryImpl (IdolCalculatorDatabase db) {
        this.db = db;
    }

    @Override
    public LiveData<List<Grade>> getGrades() {
        return db.gradeDao().getGrades();
    }

    @Override
    public LiveData<Grade> getGradeById(int id) {
        return db.gradeDao().getGradeById(id);
    }

    @Override
    public void createGrade(final Grade grade) {
        db.gradeDao().createGrade(grade);
        Logger.i("Grade", "Added");
    }

    @Override
    public void deleteGrade(final Grade grade) {
        db.gradeDao().deleteGrade(grade);
    }

    @Override
    public void updateGrade(final Grade grade) {
        db.gradeDao().updateGrade(grade);
    }
}
