package be.quirkypengu.idolcalculator.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 1/06/2017
 * @modified
 * @updated
 */

@Entity(tableName = "build")
public class Build {

    @PrimaryKey
    private int id;
    private String name;
    private String description;
    @ColumnInfo(name = "total_score")
    private int totalScore;

    @Ignore
    public Build() {

    }

    public Build(int id, String name, String description, int totalScore) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.totalScore = totalScore;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }
}
