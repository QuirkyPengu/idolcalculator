package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Insert;

import java.util.List;

import javax.inject.Inject;

import be.quirkypengu.idolcalculator.data.db.IdolCalculatorDatabase;
import be.quirkypengu.idolcalculator.data.entities.Idol;
import be.quirkypengu.idolcalculator.quirky.Logger;
import io.reactivex.Completable;
import io.reactivex.functions.Action;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 30/05/2017
 * @modified
 * @updated
 */

public class IdolRepositoryImpl implements IdolRepository {

    @Inject
    IdolCalculatorDatabase db;

    public IdolRepositoryImpl (IdolCalculatorDatabase db) {
        this.db = db;
    }

    @Override
    public LiveData<List<Idol>> getIdols() {
        return db.idolDao().getIdols();
    }

    @Override
    public LiveData<Idol> getIdolById(int id) {
        return db.idolDao().getIdolById(id);
    }

    @Override
    public LiveData<List<Idol>> getIdolsByGradeId(int id) {
        return db.idolDao().getIdolsByGradeId(id);
    }

    @Override
    public LiveData<List<Idol>> getIdolsByBossId(int id) {
        return db.idolDao().getIdolsByBossId(id);
    }

    @Override
    public void createIdol(final Idol idol) {
        db.idolDao().createIdol(idol);
        Logger.i("Idol", "Added");
    }

    @Override
    public void deleteIdol(final Idol idol) {
        db.idolDao().deleteIdol(idol);
    }

    @Override
    public void updateIdol(final Idol idol) {
        db.idolDao().updateIdol(idol);
    }
}
