package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import be.quirkypengu.idolcalculator.data.db.IdolCalculatorDatabase;
import be.quirkypengu.idolcalculator.data.entities.Boss;
import be.quirkypengu.idolcalculator.quirky.Logger;
import io.reactivex.Completable;
import io.reactivex.functions.Action;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 31/05/2017
 * @modified
 * @updated
 */

public class BossRepositoryImpl implements BossRepository {

    @Inject
    IdolCalculatorDatabase db;

    public BossRepositoryImpl (IdolCalculatorDatabase db) {
        this.db = db;
    }


    @Override
    public LiveData<List<Boss>> getBosses() {
        return db.bossDao().getBosses();
    }

    @Override
    public LiveData<Boss> getBossById(int id) {
        return db.bossDao().getBossById(id);
    }

    @Override
    public void createBoss(Boss boss) {
        db.bossDao().createBoss(boss);
        Logger.i("Boss", "Added");
    }

    @Override
    public void deleteBoss(final Boss boss) {
        db.bossDao().deleteBoss(boss);
    }

    @Override
    public void updateBoss(final Boss boss) {
        db.bossDao().updateBoss(boss);
    }
}
