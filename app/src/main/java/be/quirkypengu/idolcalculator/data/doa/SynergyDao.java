package be.quirkypengu.idolcalculator.data.doa;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Synergy;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 29/05/2017
 * @modified
 * @updated
 */

@Dao
public interface SynergyDao {
    @Query("SELECT * FROM synergy")
    LiveData<List<Synergy>> getSynergies();

    @Query("SELECT * FROM synergy WHERE id = :id")
    LiveData<Synergy> getSynergyById(int id);

    @Insert
    void createSynergy(Synergy synergy);

    @Delete
    void deleteSynergy(Synergy synergy);

    @Update
    void updateSynergy(Synergy synergy);
}
