package be.quirkypengu.idolcalculator.data.helpers;

import android.util.Log;

import java.util.Objects;

import be.quirkypengu.idolcalculator.data.entities.Boss;
import be.quirkypengu.idolcalculator.data.entities.Grade;
import be.quirkypengu.idolcalculator.data.entities.Idol;
import be.quirkypengu.idolcalculator.data.entities.Incompatibility;
import be.quirkypengu.idolcalculator.data.entities.Synergy;
import be.quirkypengu.idolcalculator.data.repository.BossRepository;
import be.quirkypengu.idolcalculator.data.repository.GradeRepository;
import be.quirkypengu.idolcalculator.data.repository.IdolRepository;
import be.quirkypengu.idolcalculator.data.repository.IncompatibilityRepository;
import be.quirkypengu.idolcalculator.data.repository.SynergyRepository;

/**
 * @author joeriverlooy
 * @version 1.0
 * @created 07/06/2017
 */

public class DataHelper {

    static final Object LOCK = new Object();

    public void initBossData(BossRepository bossRepository) {
        synchronized (LOCK) {
            bossRepository.createBoss(new Boss(0, "Korriander"));
            bossRepository.createBoss(new Boss(1, "Merkator"));
            bossRepository.createBoss(new Boss(2, "Tynril"));
            bossRepository.createBoss(new Boss(3, "Protoz'orror"));
            bossRepository.createBoss(new Boss(4, "Royal Mastogob"));
            bossRepository.createBoss(new Boss(5, "Giant Kralove"));
            bossRepository.createBoss(new Boss(6, "Missiz Freeze"));
            bossRepository.createBoss(new Boss(7, "Kolosso"));
            bossRepository.createBoss(new Boss(8, "Nileza"));
            bossRepository.createBoss(new Boss(9, "Sylargh"));
            bossRepository.createBoss(new Boss(10, "Obsidemon"));
            bossRepository.createBoss(new Boss(11, "Buck Anear"));
            bossRepository.createBoss(new Boss(12, "Kimbo"));
            bossRepository.createBoss(new Boss(13, "Vortex"));
            bossRepository.createBoss(new Boss(14, "Puppet Master"));
        }
    }

    public void initGradeData(GradeRepository gradeRepository) {
        synchronized (LOCK) {
            gradeRepository.createGrade(new Grade(0, "Minor"));
            gradeRepository.createGrade(new Grade(1, "Regular"));
            gradeRepository.createGrade(new Grade(2, "Major"));
            gradeRepository.createGrade(new Grade(3, "Great"));
        }
    }

    public void initIdolData(IdolRepository idolRepository) {
        synchronized (LOCK) {
            idolRepository.createIdol(
                    new Idol(
                            0,
                            "Nyan",
                            "Staying on the same cell at the end of a turn makes you lose 50% HP for 2 turns.",
                            "Fleeing while backing up is just like giving fierce chase to an escaping enemy. Except in reverse.",
                            10,
                            100,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178098.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            1,
                            "Ougaa",
                            "If you are on a cell adjacent to an enemy's cell at the end of the turn, you lose 50% HP.",
                            "Keep your friends close, and get the heck away from your enemies.",
                            15,
                            50,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178094.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            2,
                            "Proxima",
                            "Opponents kill enemies located on adjacent cells when they die.",
                            "Those who leave always take something with them.",
                            15,
                            30,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178085.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            3,
                            "Corrode",
                            "Opponents apply 40% additional Erosion.",
                            "Little Tofus fell great oaks.",
                            20,
                            127,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178086.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            4,
                            "Critus",
                            "Opponents have a 100% Chance of inflicting Critical Hits.",
                            "A critical mass of massive criticals.",
                            20,
                            140,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178088.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            5,
                            "Aroumb",
                            "At the end of each of an ally's turns, enemies in line with the ally finishing its turn are healed.",
                            "A healed enemy can be KO'd several times over.",
                            25,
                            80,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178102.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            6,
                            "Muta",
                            "If you are not on a cell adjacent to an enemy's cell at the end of the turn, you lose 50% HP.",
                            "Keep your friends close and your enemies closer.",
                            30,
                            60,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178093.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            7,
                            "Djim",
                            "Monsters are invulnerable as long as they do not move. Push back damages make them gain 200% damage.",
                            "He who always fights in the same spot gives up the opportunity to win elsewhere.",
                            40,
                            161,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178101.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            8,
                            "Nekineko",
                            "Opponent gains 1 MP each time they are hit.",
                            "Stop touching it!",
                            40,
                            110,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178087.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            9,
                            "Dakid",
                            "Each attack on a monster attracts all his enemies in a line or diagonally and increases his damage by 20% for 1 turn.",
                            "He who sleeps with his enemies wakes up dead.",
                            50,
                            90,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178092.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            10,
                            "Penitent",
                            "It is impossible to steal health from your opponents or to heal yourself",
                            "Only a penitent man shall pass.",
                            15,
                            123,
                            true,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178104.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            11,
                            "Hoskar",
                            "Being aligned with an enemy/ally at the end of a turn makes you lose 50% HP for 2 turns.",
                            "If the enemy is in range, so are you.",
                            20,
                            120,
                            true,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178089.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            12,
                            "Koria",
                            "A glyph is placed under each character at the start of the turn. This glyph kills at the end of the turn.",
                            "Better hurry it up.",
                            20,
                            1163,
                            true,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178088.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            13,
                            "Vood",
                            "At the end of each of an ally's turns, allies in line with the ally finishing its turn suffer damage equivalent to 50% of their HP.",
                            "If one of your allies is in line with you, he is already dead, he just doesn't know it yet.",
                            20,
                            70,
                            true,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178090.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            14,
                            "Binar",
                            "Each character located on an even cell loses 30% of their HP when an ally ends their turn on an odd cell.",
                            "01010100 01101000 01101001 01110011 00100000 01101001 01100100 01101111 01101100 00100000 01110011 01101000 01100001 01101100 01101100 00100000 01100010 01110010 01101001 01101110 01100111 00100000 01111001 01101111 01110101 00100000 01110010 01110101 01101001 01101110 00101110",
                            25,
                            170,
                            true,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178096.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            15,
                            "Ultram",
                            "Allies place a glyph at the start of their turn for an unlimited duration. If the enemies start or finish their turn on it, they become invulnerable.",
                            "For the first time ever, all your enemies want to be in your shoes.",
                            25,
                            130,
                            true,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178100.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            16,
                            "Nahuatl",
                            "When an enemy dies, they are automatically resuscitated by the next enemy in initiative order, at the start of its turn.",
                            "That which is dead may yet be killed.",
                            50,
                            180,
                            true,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178103.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            17,
                            "Minor Oaf",
                            "Your opponents' close combat damages are increased 50%.",
                            "Keep your distance.",
                            10,
                            25,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178053.png",
                            0
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            18,
                            "Oaf",
                            "Your opponents' close combat damages are increased 100%.",
                            "Keep your distance.",
                            20,
                            65,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178054.png",
                            1
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            19,
                            "Major Oaf",
                            "Your opponents' close combat damages are increased 200%.",
                            "Keep your distance.",
                            30,
                            105,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178055.png",
                            2
                    )
            );
            idolRepository.createIdol(
                    new Idol(
                            20,
                            "Great Oaf",
                            "Your opponents' close combat damages are increased 400%.",
                            "Keep your distance.",
                            40,
                            145,
                            false,
                            "http://staticns.ankama.com/dofus/www/game/items/200/178056.png",
                            3
                    )
            );
        }
    }

    public void initIncompatibilityData(IncompatibilityRepository incompatibilityRepository) {
        synchronized (LOCK) {
            incompatibilityRepository.createIncompatibility(new Incompatibility(0, 0, 0));
            incompatibilityRepository.createIncompatibility(new Incompatibility(1, 1, 1));
            incompatibilityRepository.createIncompatibility(new Incompatibility(2, 2, 1));
            incompatibilityRepository.createIncompatibility(new Incompatibility(3, 3, 2));
            incompatibilityRepository.createIncompatibility(new Incompatibility(4, 5, 3));
            incompatibilityRepository.createIncompatibility(new Incompatibility(5, 6, 1));
            incompatibilityRepository.createIncompatibility(new Incompatibility(6, 7, 3));
            incompatibilityRepository.createIncompatibility(new Incompatibility(7, 7, 4));
            incompatibilityRepository.createIncompatibility(new Incompatibility(8, 8, 5));
            incompatibilityRepository.createIncompatibility(new Incompatibility(9, 9, 6));
            incompatibilityRepository.createIncompatibility(new Incompatibility(10, 10, 7));
            incompatibilityRepository.createIncompatibility(new Incompatibility(11, 11, 8));
            incompatibilityRepository.createIncompatibility(new Incompatibility(12, 15, 0));
            incompatibilityRepository.createIncompatibility(new Incompatibility(13, 15, 3));
            incompatibilityRepository.createIncompatibility(new Incompatibility(14, 16, 3));
            incompatibilityRepository.createIncompatibility(new Incompatibility(15, 16, 9));
            incompatibilityRepository.createIncompatibility(new Incompatibility(16, 16, 10));
            incompatibilityRepository.createIncompatibility(new Incompatibility(17, 16, 11));
            incompatibilityRepository.createIncompatibility(new Incompatibility(18, 16, 12));
            incompatibilityRepository.createIncompatibility(new Incompatibility(19, 16, 13));
            incompatibilityRepository.createIncompatibility(new Incompatibility(20, 16, 14));
            incompatibilityRepository.createIncompatibility(new Incompatibility(21, 17, 2));
            incompatibilityRepository.createIncompatibility(new Incompatibility(22, 18, 2));
            incompatibilityRepository.createIncompatibility(new Incompatibility(23, 19, 2));
            incompatibilityRepository.createIncompatibility(new Incompatibility(24, 20, 2));
        }
    }

    public void initSynergyData(SynergyRepository synergyRepository) {
        synchronized (LOCK) {
            synergyRepository.createSynergy(new Synergy(0, 12, 0, 0.7));
            synergyRepository.createSynergy(new Synergy(1, 2, 1, 0.67));
            synergyRepository.createSynergy(new Synergy(2, 6, 1, 1.67));
            synergyRepository.createSynergy(new Synergy(3, 8, 1, 1.14));
            synergyRepository.createSynergy(new Synergy(4, 17, 1, 0.8));
            synergyRepository.createSynergy(new Synergy(5, 18, 1, 0.86));
            synergyRepository.createSynergy(new Synergy(6, 19, 1, 0.89));
            synergyRepository.createSynergy(new Synergy(7, 20, 1, 0.91));
            synergyRepository.createSynergy(new Synergy(8, 6, 2, 1.44));
            synergyRepository.createSynergy(new Synergy(9, 9, 2, 1.1));
            synergyRepository.createSynergy(new Synergy(10, 10, 3, 0.69));
            synergyRepository.createSynergy(new Synergy(11, 6, 5, 1.82));
            synergyRepository.createSynergy(new Synergy(12, 9, 6, 1.26));
            synergyRepository.createSynergy(new Synergy(13, 10, 6, 1.33));
            synergyRepository.createSynergy(new Synergy(14, 11, 6, 1.5));
            synergyRepository.createSynergy(new Synergy(15, 14, 6, 1.36));
            synergyRepository.createSynergy(new Synergy(16, 15, 6, 1.82));
            synergyRepository.createSynergy(new Synergy(17, 17, 6, 1.13));
            synergyRepository.createSynergy(new Synergy(18, 18, 6, 1.2));
            synergyRepository.createSynergy(new Synergy(19, 19, 6, 1.25));
            synergyRepository.createSynergy(new Synergy(20, 20, 6, 1.29));
            synergyRepository.createSynergy(new Synergy(21, 9, 8, 0.9));
            synergyRepository.createSynergy(new Synergy(22, 10, 8, 1.19));
            synergyRepository.createSynergy(new Synergy(23, 15, 8, 1.2));
            synergyRepository.createSynergy(new Synergy(24, 17, 8, 1.21));
            synergyRepository.createSynergy(new Synergy(25, 18, 8, 1.17));
            synergyRepository.createSynergy(new Synergy(26, 19, 8, 1.15));
            synergyRepository.createSynergy(new Synergy(27, 20, 8, 1.13));
            synergyRepository.createSynergy(new Synergy(28, 17, 9, 1.21));
            synergyRepository.createSynergy(new Synergy(29, 18, 9, 1.17));
            synergyRepository.createSynergy(new Synergy(30, 19, 9, 1.15));
            synergyRepository.createSynergy(new Synergy(31, 20, 9, 1.13));
            synergyRepository.createSynergy(new Synergy(32, 18, 17, 0.83));
            synergyRepository.createSynergy(new Synergy(33, 19, 17, 0.88));
            synergyRepository.createSynergy(new Synergy(34, 20, 17, 0.9));
        }
    }


}
