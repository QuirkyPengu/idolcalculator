package be.quirkypengu.idolcalculator.data.doa;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.BuildLine;
import be.quirkypengu.idolcalculator.data.entities.Idol;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 1/06/2017
 * @modified
 * @updated
 */

@Dao
public interface BuildLineDao {
    @Query("SELECT * FROM build_line")
    LiveData<List<BuildLine>> getBuildLines();

    @Query("SELECT * FROM build_line WHERE id = :id")
    LiveData<BuildLine> getBuildLineById(int id);

    @Insert
    void createBuildLine(BuildLine line);

    @Delete
    void deleteBuildLine(BuildLine line);

    @Update
    void updateBuildLine(BuildLine line);
}
