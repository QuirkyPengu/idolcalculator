package be.quirkypengu.idolcalculator.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author joeriverlooy
 * @version 1.0
 * @created 28/05/2017
 */

@Entity(tableName = "boss")
public class Boss {
    @PrimaryKey
    private int id;
    private String name;

    @Ignore
    public Boss() {
    }

    public Boss(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
