package be.quirkypengu.idolcalculator.data.doa;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Grade;
import be.quirkypengu.idolcalculator.data.entities.Idol;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 29/05/2017
 * @modified
 * @updated
 */

@Dao
public interface GradeDao {
    @Query("SELECT * FROM grade")
    LiveData<List<Grade>>  getGrades();

    @Query("SELECT * FROM grade WHERE id = :id")
    LiveData<Grade> getGradeById(int id);

    @Insert
    void createGrade(Grade grade);

    @Delete
    void deleteGrade(Grade grade);

    @Update
    void updateGrade(Grade grade);
}
