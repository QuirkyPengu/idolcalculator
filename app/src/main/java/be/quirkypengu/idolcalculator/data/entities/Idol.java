package be.quirkypengu.idolcalculator.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author joeriverlooy
 * @version 1.0
 * @created 27/05/2017
 */

@Entity(tableName = "idol",
    foreignKeys = {
        @ForeignKey(entity = Grade.class,
                parentColumns = "id",
                childColumns = "grade_id")
    }
)
public class Idol {
    @PrimaryKey
    private int id;
    private String name;
    private String effect;
    private String description;
    private int score;
    private int level;
    @ColumnInfo(name = "is_group")
    private boolean isGroup;
    @ColumnInfo(name = "image_url")
    private String imageUrl;
    @ColumnInfo(name = "grade_id")
    private int gradeId;

    @Ignore
    public Idol() {
    }

    public Idol(int id, String name, String effect, String description, int score, int level, boolean isGroup, String imageUrl, int gradeId) {
        this.id = id;
        this.name = name;
        this.effect = effect;
        this.description = description;
        this.score = score;
        this.level = level;
        this.isGroup = isGroup;
        this.imageUrl = imageUrl;
        this.gradeId = gradeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }
}
