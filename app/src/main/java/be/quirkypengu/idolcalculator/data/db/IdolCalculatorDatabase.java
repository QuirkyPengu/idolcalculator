package be.quirkypengu.idolcalculator.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import be.quirkypengu.idolcalculator.data.doa.BossDao;
import be.quirkypengu.idolcalculator.data.doa.BuildDao;
import be.quirkypengu.idolcalculator.data.doa.BuildLineDao;
import be.quirkypengu.idolcalculator.data.doa.GradeDao;
import be.quirkypengu.idolcalculator.data.doa.IdolDao;
import be.quirkypengu.idolcalculator.data.doa.IncompatibilityDao;
import be.quirkypengu.idolcalculator.data.doa.SynergyDao;
import be.quirkypengu.idolcalculator.data.entities.Boss;
import be.quirkypengu.idolcalculator.data.entities.Build;
import be.quirkypengu.idolcalculator.data.entities.BuildLine;
import be.quirkypengu.idolcalculator.data.entities.Grade;
import be.quirkypengu.idolcalculator.data.entities.Idol;
import be.quirkypengu.idolcalculator.data.entities.Incompatibility;
import be.quirkypengu.idolcalculator.data.entities.Synergy;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 29/05/2017
 * @modified
 * @updated
 */

@Database(entities = {Boss.class, Grade.class, Idol.class, Incompatibility.class, Synergy.class, Build.class, BuildLine.class}, version = 1)
public abstract class IdolCalculatorDatabase extends RoomDatabase {

    public abstract BossDao bossDao();
    public abstract GradeDao gradeDao();
    public abstract IdolDao idolDao();
    public abstract IncompatibilityDao incompatibilityDao();
    public abstract SynergyDao synergyDao();
    public abstract BuildDao buildDao();
    public abstract BuildLineDao buildLineDao();

}
