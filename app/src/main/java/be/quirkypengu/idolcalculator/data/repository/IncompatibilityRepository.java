package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Idol;
import be.quirkypengu.idolcalculator.data.entities.Incompatibility;
import io.reactivex.Completable;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 31/05/2017
 * @modified
 * @updated
 */

public interface IncompatibilityRepository {
    LiveData<List<Incompatibility>> getIncompatibilities();
    LiveData<Incompatibility> getIncompatibilityById(int id);
    void createIncompatibility(Incompatibility incompatibility);
    void deleteIncompatibility(Incompatibility incompatibility);
    void updateIncompatibility(Incompatibility incompatibility);
}
