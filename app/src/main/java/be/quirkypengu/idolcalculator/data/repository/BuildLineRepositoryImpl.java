package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

import be.quirkypengu.idolcalculator.data.db.IdolCalculatorDatabase;
import be.quirkypengu.idolcalculator.data.entities.Build;
import be.quirkypengu.idolcalculator.data.entities.BuildLine;
import io.reactivex.Completable;
import io.reactivex.functions.Action;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 1/06/2017
 * @modified
 * @updated
 */

public class BuildLineRepositoryImpl implements BuildLineRepository {

    @Inject
    IdolCalculatorDatabase db;

    public BuildLineRepositoryImpl (IdolCalculatorDatabase db) {
        this.db = db;
    }

    @Override
    public LiveData<List<BuildLine>> getBuildLines() {
        return db.buildLineDao().getBuildLines();
    }

    @Override
    public LiveData<BuildLine> getBuildLineById(int id) {
        return db.buildLineDao().getBuildLineById(id);
    }

    @Override
    public void createBuildLine(final BuildLine line) {
        db.buildLineDao().createBuildLine(line);
    }

    @Override
    public void deleteBuildLine(final BuildLine line) {
        db.buildLineDao().deleteBuildLine(line);
    }

    @Override
    public void updateBuildLine(final BuildLine line) {
        db.buildLineDao().updateBuildLine(line);
    }

}
