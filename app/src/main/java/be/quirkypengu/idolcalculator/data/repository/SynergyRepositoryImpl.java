package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

import be.quirkypengu.idolcalculator.data.db.IdolCalculatorDatabase;
import be.quirkypengu.idolcalculator.data.entities.Synergy;
import be.quirkypengu.idolcalculator.quirky.Logger;
import io.reactivex.Completable;
import io.reactivex.functions.Action;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 31/05/2017
 * @modified
 * @updated
 */

public class SynergyRepositoryImpl implements SynergyRepository {

    @Inject
    IdolCalculatorDatabase db;

    public SynergyRepositoryImpl (IdolCalculatorDatabase db) {
        this.db = db;
    }

    @Override
    public LiveData<List<Synergy>> getSynergies() {
        return db.synergyDao().getSynergies();
    }

    @Override
    public LiveData<Synergy> getSynergyById(int id) {
        return db.synergyDao().getSynergyById(id);
    }

    @Override
    public void createSynergy(final Synergy synergy) {
        db.synergyDao().createSynergy(synergy);
        Logger.i("Synergy", "Added");
    }

    @Override
    public void deleteSynergy(final Synergy synergy) {
        db.synergyDao().deleteSynergy(synergy);
    }

    @Override
    public void updateSynergy(final Synergy synergy) {
        db.synergyDao().updateSynergy(synergy);
    }
}
