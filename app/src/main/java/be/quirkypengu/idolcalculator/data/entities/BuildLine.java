package be.quirkypengu.idolcalculator.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 1/06/2017
 * @modified
 * @updated
 */

@Entity(
    tableName = "build_line",
    foreignKeys = {
        @ForeignKey(entity = Build.class,
                parentColumns = "id",
                childColumns = "build_id"),

        @ForeignKey(entity = Idol.class,
                parentColumns = "id",
                childColumns = "idol_id")
    }
)
public class BuildLine {
    @PrimaryKey
    private int id;
    @ColumnInfo(name = "build_id")
    private int buildId;
    @ColumnInfo(name = "idol_id")
    private int idolId;

    @Ignore
    public BuildLine() {

    }

    public BuildLine(int id, int buildId, int idolId) {
        this.id = id;
        this.buildId = buildId;
        this.idolId = idolId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBuildId() {
        return buildId;
    }

    public void setBuildId(int buildId) {
        this.buildId = buildId;
    }

    public int getIdolId() {
        return idolId;
    }

    public void setIdolId(int idolId) {
        this.idolId = idolId;
    }
}
