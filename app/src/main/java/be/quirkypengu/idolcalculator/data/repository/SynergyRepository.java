package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Idol;
import be.quirkypengu.idolcalculator.data.entities.Synergy;
import io.reactivex.Completable;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 31/05/2017
 * @modified
 * @updated
 */

public interface SynergyRepository {
    LiveData<List<Synergy>> getSynergies();
    LiveData<Synergy> getSynergyById(int id);
    void createSynergy(Synergy synergy);
    void deleteSynergy(Synergy synergy);
    void updateSynergy(Synergy synergy);
}
