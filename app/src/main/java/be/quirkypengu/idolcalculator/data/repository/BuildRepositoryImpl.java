package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

import be.quirkypengu.idolcalculator.data.db.IdolCalculatorDatabase;
import be.quirkypengu.idolcalculator.data.entities.Build;
import io.reactivex.Completable;
import io.reactivex.functions.Action;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 1/06/2017
 * @modified
 * @updated
 */

public class BuildRepositoryImpl implements BuildRepository {

    @Inject
    IdolCalculatorDatabase db;

    public BuildRepositoryImpl (IdolCalculatorDatabase db) {
        this.db = db;
    }

    @Override
    public LiveData<List<Build>> getBuilds() {
        return db.buildDao().getBuilds();
    }

    @Override
    public LiveData<Build> getBuildById(int id) {
        return db.buildDao().getBuildById(id);
    }

    @Override
    public void createBuild(final Build build) {
        db.buildDao().createBuild(build);
    }

    @Override
    public void deleteBuild(final Build build) {
        db.buildDao().deleteBuild(build);
    }

    @Override
    public void updateBuild(final Build build) {
        db.buildDao().updateBuild(build);
    }
}
