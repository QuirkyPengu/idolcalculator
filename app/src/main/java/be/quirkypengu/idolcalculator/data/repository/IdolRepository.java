package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Idol;
import io.reactivex.Completable;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 30/05/2017
 * @modified
 * @updated
 */

public interface IdolRepository {
    LiveData<List<Idol>> getIdols();
    LiveData<Idol> getIdolById(int id);
    LiveData<List<Idol>> getIdolsByGradeId(int id);
    LiveData<List<Idol>> getIdolsByBossId(int id);
    void createIdol(Idol idol);
    void deleteIdol(Idol idol);
    void updateIdol(Idol idol);
}
