package be.quirkypengu.idolcalculator.data.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Boss;
import be.quirkypengu.idolcalculator.data.entities.Idol;
import io.reactivex.Completable;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 31/05/2017
 * @modified
 * @updated
 */

public interface BossRepository {
    LiveData<List<Boss>> getBosses();
    LiveData<Boss> getBossById(int id);
    void createBoss(Boss boss);
    void deleteBoss(Boss boss);
    void updateBoss(Boss boss);
}
