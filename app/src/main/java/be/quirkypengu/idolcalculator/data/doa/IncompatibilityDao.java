package be.quirkypengu.idolcalculator.data.doa;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import be.quirkypengu.idolcalculator.data.entities.Idol;
import be.quirkypengu.idolcalculator.data.entities.Incompatibility;

/**
 * @author Joeri Verlooy
 * @version 1.0
 * @created 29/05/2017
 * @modified
 * @updated
 */

@Dao
public interface IncompatibilityDao {
    @Query("SELECT * FROM incompatibility")
    LiveData<List<Incompatibility>> getIncompatibilities();

    @Query("SELECT * FROM incompatibility WHERE id = :id")
    LiveData<Incompatibility> getIncompatibilityById(int id);

    @Insert
    void createIncompatibility(Incompatibility incompatibility);

    @Delete
    void deleteIncompatibility(Incompatibility incompatibility);

    @Update
    void updateIncompatibility(Incompatibility incompatibility);
}
