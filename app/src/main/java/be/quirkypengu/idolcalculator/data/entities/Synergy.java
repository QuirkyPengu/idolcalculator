package be.quirkypengu.idolcalculator.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author joeriverlooy
 * @version 1.0
 * @created 28/05/2017
 */

@Entity(
        tableName = "synergy",
        foreignKeys = {
            @ForeignKey(entity = Idol.class,
                    parentColumns = "id",
                    childColumns = "primary_idol_id"),

            @ForeignKey(entity = Idol.class,
                    parentColumns = "id",
                    childColumns = "secondary_idol_id")
        }
)
public class Synergy {
    @PrimaryKey
    private int id;
    @ColumnInfo(name = "primary_idol_id")
    private int primaryIdolId;
    @ColumnInfo(name = "secondary_idol_id")
    private int secondaryIdolId;
    private double multiplier;

    @Ignore
    public Synergy() {
    }

    public Synergy(int id, int primaryIdolId, int secondaryIdolId, double multiplier) {
        this.id = id;
        this.primaryIdolId = primaryIdolId;
        this.secondaryIdolId = secondaryIdolId;
        this.multiplier = multiplier;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrimaryIdolId() {
        return primaryIdolId;
    }

    public void setPrimaryIdolId(int primaryIdolId) {
        this.primaryIdolId = primaryIdolId;
    }

    public int getSecondaryIdolId() {
        return secondaryIdolId;
    }

    public void setSecondaryIdolId(int secondaryIdolId) {
        this.secondaryIdolId = secondaryIdolId;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }
}
